package com.example.bdstuanson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdstuansonApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdstuansonApplication.class, args);
	}

}
