package com.example.bdstuanson.config;

import com.example.bdstuanson.model.Roles;
import com.example.bdstuanson.model.User;
import com.example.bdstuanson.respository.RolesRespository;
import com.example.bdstuanson.respository.UserRespository;
import com.example.bdstuanson.service.jwt.CustomPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    private UserRespository userRepository;
    private RolesRespository roleRepository;

    @Autowired
    public DataSeedingListener(UserRespository userRepository, RolesRespository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        CustomPasswordEncoder customPasswordEncoder = new CustomPasswordEncoder();

        if (roleRepository.findByName("admin") == null) {
            roleRepository.save(new Roles("admin","admin of system"));
        }

        if (userRepository.findByUsername("admin") == null) {
            Roles roles = roleRepository.findByName("admin");
            userRepository.save(new User("admin",customPasswordEncoder.encode("admin"),"Nguyễn Tuấn Anh","tuananh@gmail.com","Ha Noi","0928088808",roles.getId()));
        }
    }
}
