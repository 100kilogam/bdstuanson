package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.bdstuanson.model.Address;
import com.example.bdstuanson.service.AddressService;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping(value = "api/v1/address/")
public class AddressController {
	@Autowired
	private AddressService service;

	@GetMapping("get-all")
	public List<Address> list() {
		return service.listAll();
	}
	@GetMapping("{id}")
	public ResponseEntity< Address> get(@PathVariable Integer id ) {
		try {
		Address address = service.get(id);
		return new ResponseEntity<Address>(address, HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<Address>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("create")
	public void add(@RequestBody Address address  ) {
		service.save(address);
	}
	@PutMapping("update/{id}")
	public ResponseEntity<?> update(@RequestBody Address address , @PathVariable Integer id ){
		try {
			Address existAddress = service.get(id);
			service.save(address);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("delete/{id}")
	public void  delete(@PathVariable Integer id ) {
		service.delete(id);
	}
	

}
