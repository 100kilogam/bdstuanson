package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.bdstuanson.model.Categories;
import com.example.bdstuanson.service.CategoriesService;
@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping(value = "api/v1/category")
public class CategoriesController {

	@Autowired
	private CategoriesService service;

		@GetMapping("/get-all")
	public List<Categories> list() {
		return service.listAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity< Categories> get(@PathVariable Integer id ) {
		try {
		Categories categories = service.get(id);
		return new ResponseEntity<Categories>(categories, HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<Categories>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/create")
	public void add(@RequestBody Categories categories ) {
		service.save(categories);
	}
	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@RequestBody Categories categories, @PathVariable Integer id ){
		try {
			Categories existCategory = service.get(id);
			service.save(categories);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/delete/{id}")
	public void  delete(@PathVariable Integer id ) {
		service.delete(id);
	}
	
	

}
