package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.bdstuanson.model.Contact;
import com.example.bdstuanson.service.ContactService;

@RestController
public class ContactController {
	@Autowired
	private ContactService service;
	@GetMapping("/contact")
	public List<Contact> list() {
		return service.listAll();
	}
	@GetMapping("/contact/{id}")
	public ResponseEntity< Contact> get(@PathVariable Integer id ) {
		try {
		Contact contact  = service.get(id);
		return new ResponseEntity<Contact>(contact, HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/contact")
	public void add(@RequestBody Contact contact ) {
		service.save(contact);
	}
	
	@DeleteMapping("/contact/{id}")
	public void  delete(@PathVariable Integer id ) {
		service.delete(id);
	}

}
