package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.bdstuanson.model.Post;
import com.example.bdstuanson.service.PostService;

@RestController
public class PostController {
	@Autowired
	private PostService service;
	@GetMapping("/post")
	public List<Post> list() {
		return service.listAll();
	}

	@GetMapping("/post/{id}")
	public ResponseEntity< Post> get(@PathVariable Integer id ) {
		try {
			Post post = service.get(id);
		return new ResponseEntity<Post>(post, HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<Post>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/post")
	public void add(@RequestBody Post post ) {
		service.save(post);
	}
	@PutMapping("/post/{id}")
	public ResponseEntity<?> update(@RequestBody Post post, @PathVariable Integer id ){
		try {
			Post existPost = service.get(id);
			service.save(post);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/post/{id}")
	public void  delete(@PathVariable Integer id ) {
		service.delete(id);
	}
	

}
