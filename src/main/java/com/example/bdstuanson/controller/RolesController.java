package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.bdstuanson.model.Roles;
import com.example.bdstuanson.service.RolesService;

@RestController
public class RolesController {

	@Autowired
	private RolesService service;

	@GetMapping("/roles")
	public List<Roles> list() {
		return service.listAll();
	}
	@GetMapping("/roles/{id}")
	public ResponseEntity< Roles> get(@PathVariable Integer id ) {
		try {
		Roles roles  = service.get(id);
		return new ResponseEntity<Roles>(roles, HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<Roles>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/roles")
	public void add(@RequestBody Roles roles ) {
		service.save(roles);
	}
	@PutMapping("/roles/{id}")
	public ResponseEntity<?> update(@RequestBody Roles roles , @PathVariable Integer id ){
		try {
			Roles existRoles = service.get(id);
			service.save(roles);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/roles/{id}")
	public void  delete(@PathVariable Integer id ) {
		service.delete(id);
	}
}
