package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import com.example.bdstuanson.config.StatusCode;
import com.example.bdstuanson.dto.bo.Response;
import com.example.bdstuanson.dto.bo.SystemResponse;
import com.example.bdstuanson.dto.in.UserRequest;
import com.example.bdstuanson.dto.out.BaseResponse;
import com.example.bdstuanson.dto.out.LoginResponse;
import com.example.bdstuanson.service.jwt.JwtTokenProvider;
import com.example.bdstuanson.service.jwt.model.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import com.example.bdstuanson.model.User;
import com.example.bdstuanson.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	private UserService service;

	@GetMapping("/user")
	public List<User> listAll() {
		return service.listAll();

	}

	@GetMapping("/user/{id}")
	public ResponseEntity<User> get(@PathVariable Integer id) {
		try {
			User user = service.get(id);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/user")
	public void add(@RequestBody User user) {
		service.save(user);
	}

	@PutMapping("/user/{id}")
	public ResponseEntity<?> update(@RequestBody User user, @PathVariable Integer id) {
		try {
			User existUser = service.get(id);
			service.save(user);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/user/{id}")
	public void delete(@PathVariable Integer id) {
		service.delete(id);
	}

	// @PostMapping("/user/login")
	// public ResponseEntity<SystemResponse<BaseResponse>> login(@RequestBody
	// UserRequest userRequest){
	// return service.login(userRequest);
	// }
	@PostMapping("/user/login")
	public ResponseEntity<SystemResponse<BaseResponse>> login(@RequestBody UserRequest userRequest) {
		return service.login(userRequest);
	}
}
