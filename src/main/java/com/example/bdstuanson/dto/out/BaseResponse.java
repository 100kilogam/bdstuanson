package com.example.bdstuanson.dto.out;

import com.example.bdstuanson.config.StatusCode;

public class BaseResponse {

    private int errorCode;
    private Object message;
    private Object data;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public BaseResponse() {
        this.errorCode = StatusCode.ERROR;
        this.message = "Error";
    }

}
