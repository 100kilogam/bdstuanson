package com.example.bdstuanson.dto.out;


import com.example.bdstuanson.model.User;


public class LoginResponse {

    private String token;
    private User userEntity;

    public LoginResponse() {
    }

    public LoginResponse(String token, User userEntity) {
        this.token = token;
        this.userEntity = userEntity;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(User userEntity) {
        this.userEntity = userEntity;
    }
}
