package com.example.bdstuanson.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "t_categorys")

public class Categories extends BaseEntity {
	 
		
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2594541429651905924L;

	@Column(name = "name", length = 45, nullable = false, unique = true)
    private String name;
	@Column(name = "description", length = 200, nullable = false)
	private String description;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	

}
