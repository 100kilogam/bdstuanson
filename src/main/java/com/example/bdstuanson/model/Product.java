package com.example.bdstuanson.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_product")
public class Product extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2078405232904480996L;
	@Column(name = "name", length = 200, nullable = false, unique = true)
    private String name;
	@Column(name = "acreage", length = 45, nullable = false)
	private String acreage;
	@Column(name = "price", length = 45, nullable = false)
	private String price;
	@Column(name = "description", length = 200, nullable = false)


	private String description;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAcreage() {
		return acreage;
	}
	public void setAcreage(String acreage) {
		this.acreage = acreage;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
