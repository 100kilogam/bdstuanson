package com.example.bdstuanson.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name= "t_roles")
public class Roles extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3306704033146103369L;
	
	
	@Column(name = "name", length = 45, nullable = false, unique = true)
    private String name;
	@Column(name = "description", length = 200, nullable = false)
	private String description;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Roles() {
	}

	public Roles(String name, String description) {
		this.name = name;
		this.description = description;
	}
}
