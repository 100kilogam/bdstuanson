package com.example.bdstuanson.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="t_user")
@Data
public class User extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5264157086457467953L;
	@Column(name = "username", length = 45, nullable = false)

	private String username;
	@Column(name = "password", length = 500, nullable = false)

	private String password;
	@Column(name = "full_name", length = 200, nullable = false)

	private String fullName;
	@Column(name = "email", length = 45, nullable = false)

	private String email;
	@Column(name = "address", length = 45, nullable = false)

	private String address;
	@Column(name = "mobile", length = 45, nullable = false)

	private String mobile;

	private Integer roleId;

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;

	}

	public User() {
	}

	public User(String username, String password, String fullName, String email, String address, String mobile, Integer roleId) {
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.email = email;
		this.address = address;
		this.mobile = mobile;
		this.roleId = roleId;
	}
}
