package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.Address;

@Repository
public interface AddressRespository extends JpaRepository<Address, Integer> {

}
