package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.Categories;

@Repository
public interface CategoriesRespository extends JpaRepository<Categories, Integer>{

}
