package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.Contact;

@Repository
public interface ContactRespository extends JpaRepository<Contact, Integer>{

}
