package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.Post;

@Repository
public interface PostRespository extends JpaRepository<Post, Integer> {

}
