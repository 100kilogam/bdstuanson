package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.Roles;

@Repository
public interface RolesRespository extends JpaRepository<Roles, Integer> {

    Roles findByName(String name);
}
