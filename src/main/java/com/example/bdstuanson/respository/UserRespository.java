package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.User;

@Repository
public interface UserRespository extends JpaRepository<User, Integer> {
	User findByUsername(String username);

	User findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}
