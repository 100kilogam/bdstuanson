package com.example.bdstuanson.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.Address;
//import com.example.bdstuanson.respository.AddressRespository;
import com.example.bdstuanson.respository.AddressRespository;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
	@Autowired
	private AddressRespository respository;
	public List<Address> listAll(){
		return respository.findAll();
	}
	
	public void save(Address address) {
		respository.save(address);
	}
	
	public Address get(Integer id) {
		return respository.findById(id).get();
	
	}
	public void delete(Integer id ) {
		respository.deleteById(id);
	}
}
