package com.example.bdstuanson.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.Contact;
import com.example.bdstuanson.respository.ContactRespository;

@Service
public class ContactService {

	@Autowired
	private ContactRespository respository;
	
	public List< Contact> listAll(){
		return respository.findAll();
		
	}
	public void save(Contact contact) {
		respository.save(contact);
	}
	
	public Contact get(Integer id) {
		return respository.findById(id).get();
	
	}
	public void delete(Integer id ) {
		respository.deleteById(id);
	}
}
	


