package com.example.bdstuanson.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.Post;
import com.example.bdstuanson.respository.PostRespository;

@Service
public class PostService {

	@Autowired
	private PostRespository respository;
	public List<Post> listAll(){
		return respository.findAll();
	}
	
	public void save(Post post) {
		respository.save(post);
	}
	
	public Post get(Integer id) {
		return respository.findById(id).get();
	
	}
	public void delete(Integer id ) {
		respository.deleteById(id);
	}
}
