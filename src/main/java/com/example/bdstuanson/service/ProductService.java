package com.example.bdstuanson.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.Product;
import com.example.bdstuanson.respository.ProductRespository;

@Service
public class ProductService {

	@Autowired
	private ProductRespository respository;
	public List<Product> listAll(){
		return respository.findAll();
	}
	
	public void save(Product product) {
		respository.save(product);
	}
	
	public Product get(Integer id) {
		return respository.findById(id).get();
	
	}
	public void delete(Integer id ) {
		respository.deleteById(id);
	}
}
