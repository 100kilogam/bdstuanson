package com.example.bdstuanson.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.Roles;
import com.example.bdstuanson.respository.RolesRespository;

@Service
public class RolesService {
	@Autowired
	private RolesRespository respository;
	public List<Roles> listAll(){
		return respository.findAll();
	}
	
	public void save(Roles roles) {
		respository.save(roles);
	}
	
	public Roles get(Integer id) {
		return respository.findById(id).get();
	
	}
	public void delete(Integer id ) {
		respository.deleteById(id);
	}

}
