package com.example.bdstuanson.service;

import java.util.List;

import com.example.bdstuanson.config.StatusCode;
import com.example.bdstuanson.dto.bo.Response;
import com.example.bdstuanson.dto.bo.SystemResponse;
import com.example.bdstuanson.dto.in.UserRequest;
import com.example.bdstuanson.dto.out.BaseResponse;
import com.example.bdstuanson.dto.out.LoginResponse;
import com.example.bdstuanson.service.jwt.JwtTokenProvider;
import com.example.bdstuanson.service.jwt.model.CustomUserDetails;
import com.example.bdstuanson.util.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.User;
import com.example.bdstuanson.respository.UserRespository;

@Service
public class UserService {

	@Autowired
	private UserRespository respository;
	@Autowired
	private UserRespository userRespository;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	JwtTokenProvider jwtTokenProvider;

	public List<User> listAll() {
		return respository.findAll();
	}

	public void save(User user) {
		respository.save(user);
	}

	public User get(Integer id) {
		return respository.findById(id).get();

	}

	public void delete(Integer id) {
		respository.deleteById(id);
	}

	public User checkLogin(String userName, String pass) {
		User result = null;
		try {
			result = userRespository.findByUsernameAndPassword(userName, EncryptUtils.crc32PasswordEncoder(pass));
			if (result != null) {
				return result;
			}

		} catch (Exception e) {
			System.out.println("error when checklogin with username [{}] and pass [{}]: {}");
			e.printStackTrace();
		}
		return result;
	}

	public ResponseEntity<SystemResponse<BaseResponse>> login(UserRequest userRequest) {
		BaseResponse baseResponse = new BaseResponse();

		if (userRequest == null || userRequest.getUsername().isEmpty() || userRequest.getPassword().isEmpty()) {
			baseResponse.setMessage(" Username and password must not null or empty ");
			baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
			return Response.badRequest("Username and password must not null or empty ");
		}

		User user = this.checkLogin(userRequest.getUsername(), userRequest.getPassword());
		if (user == null) {
			baseResponse.setMessage("Username or password in incorrect");
			baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
			return Response.badRequest("Username or password in incorrect");
		}

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(userRequest.getUsername(), userRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtTokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
		System.out.printf("=====================>", jwt);
		LoginResponse loginResponse = new LoginResponse(jwt, user);
		baseResponse.setMessage(" thanh cong");
		baseResponse.setData(loginResponse);
		baseResponse.setErrorCode(StatusCode.SUCCESS);
		return Response.ok("success", baseResponse);
	}
}
