package com.example.bdstuanson.service.jwt.model;

import com.example.bdstuanson.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class CustomUserDetails implements UserDetails {

    User account; // tao ra 1 account

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")); // ?????
    }

    public CustomUserDetails(User account){
        this.account = account;
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() { // kiem tra xem account het han chua
        return true;
    }

    @Override
    public boolean isAccountNonLocked() { // kiem tra xem account co bi khoa khong
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getAccount(){
        return account;
    }

    public void setAccount(User account) {
        this.account = account;
    }
}
