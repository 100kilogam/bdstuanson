package com.example.bdstuanson.service.jwt.service;

import com.example.bdstuanson.model.User;
import com.example.bdstuanson.respository.UserRespository;
import com.example.bdstuanson.service.jwt.model.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserJwtService implements UserDetailsService {

    @Autowired
    private UserRespository userRespository; // tao ra account

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User accountEntity = userRespository.findByUsername(username);

        if (accountEntity == null) {
            throw new UsernameNotFoundException(username);
        } else return new CustomUserDetails(accountEntity);
    }

    public CustomUserDetails loadUserByUserId(Integer userId) {
        User accountEntity = userRespository.findById(userId).orElse(null);
        if (accountEntity == null) {
            throw new UsernameNotFoundException("userId" + userId);
        } else return new CustomUserDetails(accountEntity);
    }
}
