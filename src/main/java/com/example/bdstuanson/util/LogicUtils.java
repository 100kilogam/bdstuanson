package com.example.bdstuanson.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tudv on 2019/09/16
 */
public class LogicUtils {
    public boolean checkNullOrEmpty(String value) {
        if (value == null || value.isEmpty()) {
            return true;
        } else return false;
    }

    public boolean checkNullOrEmpty(String... values) {
        for (String value : values) {
            if (checkNullOrEmpty(value) == true) {
                return true;
            }
        }
        return false;
    }

    public boolean isThisDateValid(String dateFromat, String... dateToValidate) {
        if (dateToValidate == null) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            for (String date : dateToValidate) {
                if (checkNullOrEmpty(date))
                    sdf.parse(date);
                else return false;
            }

        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public boolean checkValidPhoneNo(String phoneNo) {
        if (phoneNo.length() < 9 || phoneNo.length() > 12) {
            return false;
        }
//        else {
//            String pattern = "^[0-9]{10}$";
//            boolean match = phoneNo.matches(pattern);
//            return match;
//        }
        return true;
    }

    public boolean checkValidEmail(String email) {
        if (checkNullOrEmpty(email) == true) {
            return false;
        } else {
//            String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$";
            String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);

            return matcher.matches();
        }
    }

    public boolean checkTimeLogic(Date time1, Date time2) {
        if (time1 == null || time2 == null) {
            return false;
        }
        if (time1.after(time2)) {
            return false;
        } else {
            return true;
        }
    }
}
