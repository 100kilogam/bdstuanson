package com.example.bdstuanson.util;

public final class StringResponse {

  private StringResponse() {
  }

  public static final String FILE_TOO_LARGE = "File to large !";
  public static final String ACCOUNT_NOT_ACTIVE_OR_NOT_EXIST = "Account not active or not exist.";
  public static final String OK = "SUCCESS";
  public static final String ACCOUNT_NOT_EXIST = "This account does not exist!";
  public static final String WRONG_PASSWORD = "Wrong password";
  public static final String CHANGE_PASSWORD_SUCCESS = "Thay đổi mật khẩu thành công";
  public static final String PHONE_NUMBER_ALREADY_EXISTS = "Phone number is taken";
  public static final String ACCOUNT_LOCKED = "Account is locked !";
  public static final String PERMISSION_DENIED = "Không có quyền !!!";
}

